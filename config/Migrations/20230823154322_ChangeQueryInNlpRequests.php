<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQueryInNlpRequests extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table(\Ai\AiPlugin::getTablePrefix() . 'nlp_requests', ['collation' => 'utf8mb4_general_ci']);
        $table->changeColumn('query', 'text', [
            'null' => false,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
        ]);
        $table->update();
    }
}
