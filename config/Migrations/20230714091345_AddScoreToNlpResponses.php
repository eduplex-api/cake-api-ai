<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddScoreToNlpResponses extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table(\Ai\AiPlugin::getTablePrefix() . 'nlp_responses', ['collation' => 'utf8mb4_general_ci']);
        $table->addColumn('ai_score', 'integer', [
            'after' => 'is_ai_generated',
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
