<?php

declare(strict_types = 1);

use Migrations\AbstractMigration;

class CreateNlpLogsTables extends AbstractMigration
{
    public function change()
    {
        $tableRequests = $this
            ->table(\Ai\AiPlugin::getTablePrefix() . 'nlp_requests', ['collation' => 'utf8mb4_general_ci'])
            ->addColumn('query', 'string', [
                'null' => false,
            ])
            ->addColumn('entity', 'string', [
                'null' => false,
            ])
            ->addColumn('entity_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'null' => false,
            ])
            ->addColumn('deleted', 'datetime', [
                'null' => true,
            ]);
        $tableRequests->addIndex(['user_id', 'entity_id']);
        $tableRequests->create();

        $tableResponses = $this
            ->table(\Ai\AiPlugin::getTablePrefix() . 'nlp_responses', ['collation' => 'utf8mb4_general_ci'])
            ->addColumn('request_id', 'integer', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('is_accepted', 'boolean', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('is_ai_generated', 'boolean', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('uri', 'string', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('skill', 'string', [
                'default' => null,
                'null' => false,
            ]);
        $tableResponses->addIndex(['request_id']);
        $tableResponses->create();


    }
}
