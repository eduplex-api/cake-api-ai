<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeAiScoreInNlpResponses extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table(\Ai\AiPlugin::getTablePrefix() . 'nlp_responses', ['collation' => 'utf8mb4_general_ci']);
        $table->changeColumn('ai_score', 'float', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
