<?php
declare(strict_types=1);

namespace Ai\Test\Fixture;

use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\Fixture\RestApiFixture;

class NlpRequestsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Ai.NlpRequests';
    const NLP_REQUEST_ID = 10;

    public $records = [
        [
            'id' => self::NLP_REQUEST_ID,
            'query' => 'Blablabla',
            'entity' => 'Service',
            'entity_id' => 50,
            'user_id' => UsersFixture::SELLER_ID,
            'created' => '2023-05-11 12:00:00',
            'modified' => '2023-05-1 13:00:00',
            'deleted' => null,
        ],
    ];
}
