<?php
declare(strict_types=1);

namespace Ai\Test\Fixture;

use RestApi\TestSuite\Fixture\RestApiFixture;

class NlpResponsesFixture extends RestApiFixture
{
    const LOAD = 'plugin.Ai.NlpResponses';
    const NLP_RESPONSE_ID = 100;

    public $records = [
        [
            'id' => self::NLP_RESPONSE_ID,
            'request_id' => NlpRequestsFixture::NLP_REQUEST_ID,
            'is_accepted' => 0,
            'is_ai_generated' => 1,
            'ai_score' => 55,
            'uri' => 'www.courseticket.com/skillOne',
            'skill' => 'Skill one',
        ],
        [
            'id' => 101,
            'request_id' => NlpRequestsFixture::NLP_REQUEST_ID,
            'is_accepted' => 0,
            'is_ai_generated' => 1,
            'ai_score' => 0,
            'uri' => 'www.courseticket.com/skillTwo',
            'skill' => 'Skill two',
        ],
        [
            'id' => 102,
            'request_id' => NlpRequestsFixture::NLP_REQUEST_ID,
            'is_accepted' => 1,
            'is_ai_generated' => 1,
            'uri' => 'www.courseticket.com/skillThree',
            'skill' => 'Skill three',
        ],
        [
            'id' => 103,
            'request_id' => NlpRequestsFixture::NLP_REQUEST_ID,
            'is_accepted' => 0,
            'is_ai_generated' => 1,
            'uri' => 'www.courseticket.com/skillFour',
            'skill' => 'Skill four',
        ],
        [
            'id' => 104,
            'request_id' => NlpRequestsFixture::NLP_REQUEST_ID,
            'is_accepted' => 0,
            'is_ai_generated' => 1,
            'uri' => 'www.courseticket.com/skillFive',
            'skill' => 'Skill five',
        ],
    ];
}
