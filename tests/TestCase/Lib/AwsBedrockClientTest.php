<?php
namespace Ai\Test\TestCase\Lib;

use Ai\Lib\AwsBedrockClient;
use Cake\TestSuite\TestCase;

class AwsBedrockClientTest extends TestCase
{
    private $bedrockClient;

    public function setUp(): void
    {
        parent::setUp();
        $this->bedrockClient = new AwsBedrockClient();
    }

    public function testInvokeModel_shouldVectorizeText()
    {
        $inputText = 'css';
        $expectedTokenCount = 2;
        $dimension = 1024;

        $res = $this->bedrockClient->invokeModel($inputText, $dimension);

        $this->assertEquals($dimension, count($res->getEmbedding()));
        $this->assertEquals($expectedTokenCount, $res->getInputTextTokenCount());
    }
}
