<?php

declare(strict_types = 1);

namespace Ai\Test\TestCase\Controller;

use Ai\AiPlugin;
use Ai\Test\Fixture\NlpRequestsFixture;
use Ai\Test\Fixture\NlpResponsesFixture;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class AiNlpRequestsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        NlpRequestsFixture::LOAD, NlpResponsesFixture::LOAD, OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AiPlugin::getRoutePath() . '/users/' . UsersFixture::SELLER_ID . '/nlpRequests/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testAddNew_shouldCreateRequestAndResponses()
    {
        $expected = $this->_getExpected();
        $data = [
            'query' => 'rider',
            'service_id' => 50,
            'version' => '1.1',
            'nlp_responses' => [
                [
                    'is_accepted' => 0,
                    'is_ai_generated' => 1,
                    'ai_score' => 0.3123,
                    'uri' => 'www.courseticket.com/bmxrider',
                    'skill' => 'bmx rider',
                ],
                [
                    'is_accepted' => 1,
                    'is_ai_generated' => 1,
                    'ai_score' => 0.2231,
                    'uri' => 'www.courseticket.com/endurorider',
                    'skill' => 'enduro rider',
                ]
            ]
        ];

        $this->post($this->_getEndpoint(), $data);

        $res = $this->assertJsonResponseOK();
        $expected['data']['created'] = $res['data']['created'];
        $this->assertEquals($expected, $res);
    }

    private function _getExpected()
    {
        return [
            'data' => [
                'id' => 11,
                'query' => 'rider',
                'entity' => 'Service',
                'entity_id' => 50,
                'user_id' => UsersFixture::SELLER_ID,
                'version' => '1.1',
                'created' => '2023-05-11 12:00:00',
                'nlp_responses' => [
                    [
                        'id' => 105,
                        'request_id' => 11,
                        'is_accepted' => false,
                        'is_ai_generated' => true,
                        'ai_score' => 0.3123,
                        'uri' => 'www.courseticket.com/bmxrider',
                        'skill' => 'bmx rider',
                    ],
                    [
                        'id' => 106,
                        'request_id' => 11,
                        'is_accepted' => true,
                        'is_ai_generated' => true,
                        'ai_score' => 0.2231,
                        'uri' => 'www.courseticket.com/endurorider',
                        'skill' => 'enduro rider',
                    ],
                ]
            ]
        ];
    }
}
