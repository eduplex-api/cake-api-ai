<?php
declare(strict_types=1);

namespace Ai\Test\TestCase\Controller;

use Ai\AiPlugin;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;
use SbertService\Lib\SbertService;

class AiMatchCourseSkillsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        UsersFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AiPlugin::getRoutePath() . '/sbert/matchCourseSkills/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testAddNew_shouldMatchCourseSkills()
    {
        $expected = $this->_getExpected();
        $this->mockService(SbertService::class, function () use ($expected) {
            $chatClientMock = $this->createMock(SbertService::class);
            $chatClientMock->expects($this->once())->method('vectoriseDirectToPy')
                ->willReturn($expected);
            return $chatClientMock;
        });

        $data = [
            'language' => 'en',
            'sanitize_all' => true,
            'title' => '<p>ZusammenfassungenManage musical staff</p>',
            'description' => '<p>Manage <b>musical</b> staff</p><p>Assign tasks</p>',
            'learning_goals' => '<p>Manage musical</p><p>staff</p>'
        ];

        $this->post($this->_getEndpoint(), $data);
        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    private function _getExpected(): array
    {
        return [
            'desc_skills' => [
                '0' => [
                    'description' => 'Assign and manage staff tasks in areas such as scoring, arranging, copying music and vocal coaching.',
                    'score' => '0.9648',
                    'skill' => 'manage musical staff',
                    'skill_id' => '1',
                    'uri' => 'http://data.europa.eu/esco/skill/0005c151-5b5a-4a66-8aac-60e734beb1ab'
                ],
                '1' => [
                    'description' => 'Schedule rehearsals and music performances, arrange details such as locations, select accompanists and instrumentalists.',
                    'score' => '0.9026',
                    'skill' => 'plan musical performances',
                    'skill_id' => '3557',
                    'uri' => 'http://data.europa.eu/esco/skill/4109c79f-0332-498d-a967-b6d22761c639'
                ],
                '2' => [
                    'description' => 'Set the date, the agenda, gather the required resources, and coordinate events around music such as concerts, competitions or exams.',
                    'score' => '0.8896',
                    'skill' => 'organise musical events',
                    'skill_id' => '11068',
                    'uri' => 'http://data.europa.eu/esco/skill/cbe0922b-7fe0-4fc0-a7c7-bdca3732b78d'
                ],
                '3' => [
                    'description' => 'Direct music groups, individual musicians or complete orchestras at rehearsals and during live or studio performances, in order to improve the overall tonal and harmonic balance, dynamics, rhythm, and tempo.',
                    'score' => '0.8891',
                    'skill' => 'supervise music groups',
                    'skill_id' => '2858',
                    'uri' => 'http://data.europa.eu/esco/skill/339f165c-0002-47d2-91a6-ca4722ec682f'
                ],
                '4' => [
                    'description' => 'Guide musicians during rehearsals, live performances or studio recording sessions.',
                    'score' => '0.8856',
                    'skill' => 'supervise musicians',
                    'skill_id' => '9087',
                    'uri' => 'http://data.europa.eu/esco/skill/a672418c-0188-409e-80fb-51041f40f201'
                ]
            ],
            'goals_skills' => [
                '0' => [
                    'description' => 'Assign and manage staff tasks in areas such as scoring, arranging, copying music and vocal coaching.',
                    'score' => '0.9325',
                    'skill' => 'manage musical staff',
                    'skill_id' => '1',
                    'uri' => 'http://data.europa.eu/esco/skill/0005c151-5b5a-4a66-8aac-60e734beb1ab'
                ],
                '1' => [
                    'description' => 'Oversee all aspects of studio resourcing, such as the management of the creative staff and monitoring the workload in order to ensure the appropriate staffing levels are maintained.',
                    'score' => '0.8885',
                    'skill' => 'manage studio resourcing',
                    'skill_id' => '480',
                    'uri' => 'http://data.europa.eu/esco/skill/094bc577-0389-4cbb-9c28-c0888c5942e8'
                ],
                '2' => [
                    'description' => 'Direct music groups, individual musicians or complete orchestras at rehearsals and during live or studio performances, in order to improve the overall tonal and harmonic balance, dynamics, rhythm, and tempo.',
                    'score' => '0.8800',
                    'skill' => 'supervise music groups',
                    'skill_id' => '2858',
                    'uri' => 'http://data.europa.eu/esco/skill/339f165c-0002-47d2-91a6-ca4722ec682f'
                ],
                '3' => [
                    'description' => 'Guide musicians during rehearsals, live performances or studio recording sessions.',
                    'score' => '0.8793',
                    'skill' => 'supervise musicians',
                    'skill_id' => '9087',
                    'uri' => 'http://data.europa.eu/esco/skill/a672418c-0188-409e-80fb-51041f40f201'
                ],
                '4' => [
                    'description' => 'Manage, schedule and run rehearsals for the performance.',
                    'score' => '0.8751',
                    'skill' => 'organise rehearsals',
                    'skill_id' => '266',
                    'uri' => 'http://data.europa.eu/esco/skill/05bc7677-5a64-4e0c-ade3-0140348d4125'
                ]
            ],
            'title_skills' => [
                '0' => [
                    'distance' => '0',
                    'score' => '1.0000',
                    'skill' => 'manage musical staff',
                    'skill_id' => '1',
                    'uri' => 'http://data.europa.eu/esco/skill/0005c151-5b5a-4a66-8aac-60e734beb1ab'
                ],
                '1' => [
                    'distance' => '15',
                    'score' => '0.9243',
                    'skill' => 'supervise musicians',
                    'skill_id' => '9087',
                    'uri' => 'http://data.europa.eu/esco/skill/a672418c-0188-409e-80fb-51041f40f201'
                ],
                '2' => [
                    'distance' => '8',
                    'score' => '0.9228',
                    'skill' => 'manage staff',
                    'skill_id' => '2857',
                    'uri' => 'http://data.europa.eu/esco/skill/339ac029-066a-4985-9f9d-b3d7c8fea0bb'
                ],
                '3' => [
                    'distance' => '16',
                    'score' => '0.9064',
                    'skill' => 'supervise music groups',
                    'skill_id' => '2858',
                    'uri' => 'http://data.europa.eu/esco/skill/339f165c-0002-47d2-91a6-ca4722ec682f'
                ],
                '4' => [
                    'distance' => '13',
                    'score' => '0.9030',
                    'skill' => 'manage personnel',
                    'skill_id' => '6005',
                    'uri' => 'http://data.europa.eu/esco/skill/6d25ef0c-9f6c-4e46-acf3-b2ae022f85f6'
                ]
            ]
        ];
    }
}
