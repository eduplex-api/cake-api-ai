<?php
declare(strict_types=1);

namespace Ai\Test\TestCase\Controller;

use Ai\AiPlugin;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;
use SbertService\Lib\SbertService;

class AiPreComputedCompareSkillsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        UsersFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AiPlugin::getRoutePath() . '/sbert/preComputedCompareSkills/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testAddNew_shouldGetPreComputedCompareSkills()
    {
        $expected = $this->_getExpected();
        $this->mockService(SbertService::class, function () use ($expected) {
            $sbertServiceMock = $this->createMock(SbertService::class);
            $sbertServiceMock->expects($this->once())->method('vectoriseDirectToPy')
                ->willReturn($expected);
            return $sbertServiceMock;
        });

        $data = [
            'language' => 'en',
            'skills' => 'Manage musical staff',
            'skills_eval' => [
                'Manage musical staff',
                'supervise correctional procedures',
                'apply anti-oppressive practices'
            ]
        ];
        $this->post($this->_getEndpoint(), $data);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    private function _getExpected(): array
    {
        return [];
    }
}
