<?php
declare(strict_types=1);

namespace Ai\Test\TestCase\Controller;

use Ai\AiPlugin;
use AiTutor\Test\TestHelpers\TestContentVector;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;
use SbertService\Lib\SbertService;

class AiVectoriseControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        UsersFixture::LOAD, OauthAccessTokensFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AiPlugin::getRoutePath() . '/sbert/vectorise/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testAddNew_shouldVectorizeText()
    {
        $expected = [
            [
                'dimension' => 1024,
                'language' => 'en',
                'llm' => 'thenlper/gte-large',
                'text' => 'Examine images taken by telescopes',
                'vector' => []
            ],
        ];
        $this->mockService(SbertService::class, function () use ($expected) {
            $chatClientMock = $this->createMock(SbertService::class);
            $chatClientMock->expects($this->once())->method('vectoriseDirectToPy')
                ->willReturn($expected);
            return $chatClientMock;
        });

        $query = 'Examine images taken by telescopes';
        $payload = [
            'language' => 'en',
            'vectorise' => [$query]
        ];
        $this->post($this->_getEndpoint(), $payload);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }
}
