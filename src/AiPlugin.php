<?php
declare(strict_types=1);

namespace Ai;

use Ai\Controller\AiPreComputedCompareSkillsController;
use Cake\Core\ContainerInterface;
use Cake\Http\ServerRequest;
use Cake\Routing\RouteBuilder;
use RestApi\Lib\RestPlugin;
use SbertService\Lib\SbertService;

class AiPlugin extends RestPlugin
{
    protected function routeConnectors(RouteBuilder $builder): void
    {
        // https://docs.eduplex.eu/text-analysis/report/software-design/endpoints
        // vectorise to create embeddings (to be replaced with bedrock PRD 9407
        $builder->connect('/sbert/vectorise/*', \Ai\Controller\AiVectoriseController::route());
        // compare ESCO skills obtaining similarity scores (PcSearchTagMatch.vue)
        $builder->connect('/sbert/computeCompareSkills/*', \Ai\Controller\AiComputeCompareSkillsController::route());
        $builder->connect('/sbert/preComputedCompareSkills/*', AiPreComputedCompareSkillsController::route());
        // match course skills to get ESCO skills from text (use case auto tag events)
        $builder->connect('/sbert/matchCourseSkills/*', \Ai\Controller\AiMatchCourseSkillsController::route());
        // log nlp requests to the database
        $builder->connect('/users/{userID}/nlpRequests/*', \Ai\Controller\AiNlpRequestsController::route());
        // openapi / swagger docs
        $builder->connect('/openapi/*', \Ai\Controller\SwaggerJsonController::route());
    }

    public function services(ContainerInterface $container): void
    {
        $container->addShared(SbertService::class);// addShared means singleton
        $container->add(\Ai\Controller\AiVectoriseController::class)
            ->addArguments([SbertService::class, ServerRequest::class]);
        $container->add( \Ai\Controller\AiMatchCourseSkillsController::class)
            ->addArguments([SbertService::class, ServerRequest::class]);
        $container->add(\Ai\Controller\AiComputeCompareSkillsController::class)
            ->addArguments([SbertService::class, ServerRequest::class]);
        $container->add(AiPreComputedCompareSkillsController::class)
            ->addArguments([SbertService::class, ServerRequest::class]);
    }
}
