<?php
declare(strict_types=1);

namespace Ai\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property integer request_id
 * @property boolean user_accepted
 * @property boolean ai_generated
 * @property string uri
 * @property string skill
 */
class NlpResponse extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'is_accepted' => true,
        'is_ai_generated' => true,
        'uri' => true,
        'skill' => true,
        'ai_score' => true,
    ];

    protected $_hidden = [
    ];
}
