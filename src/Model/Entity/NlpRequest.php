<?php
declare(strict_types=1);

namespace Ai\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property string query
 * @property string entity
 * @property integer entity_id
 * @property integer user_id
 * @property string version
 */
class NlpRequest extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'query' => true,
        'version' => true,
        'nlp_responses' => true,
    ];

    protected $_hidden = [
        'modified', 'deleted',
    ];
}
