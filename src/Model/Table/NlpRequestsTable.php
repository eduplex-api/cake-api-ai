<?php

declare(strict_types = 1);

namespace Ai\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use RestApi\Model\Table\RestApiTable;

class NlpRequestsTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        NlpResponsesTable::addBelongsTo($this)->setForeignKey('request_id');
    }

    public static function load(): self
    {
        /** @var NlpRequestsTable $table */
        $table = parent::load();
        return $table;
    }
}
