<?php

declare(strict_types = 1);

namespace Ai\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use RestApi\Model\Table\RestApiTable;

class NlpResponsesTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        NlpRequestsTable::addHasMany($this)->setForeignKey('request_id');
    }

    public static function load(): self
    {
        /** @var NlpResponsesTable $table */
        $table = parent::load();
        return $table;
    }
}
