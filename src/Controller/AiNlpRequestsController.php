<?php
declare(strict_types=1);

namespace Ai\Controller;
use Ai\Model\Entity\NlpRequest;
use Ai\Model\Table\NlpRequestsTable;
use App\Controller\ApiController;
use SbertService\Lib\QueryParser;

/**
 * @property NlpRequestsTable $NlpRequests
 */
class AiNlpRequestsController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->NlpRequests = NlpRequestsTable::load();
    }

    protected function addNew($data)
    {
        $userId = $this->request->getParam('userID');
        $request = $this->NlpRequests->newEmptyEntity();
        /** @var NlpRequest $request */
        $request = $this->NlpRequests->patchEntity(
            $request, $data, ['associated' => ['NlpResponses']]);
        $request->user_id = $userId;
        $request->entity = 'Service';
        $request->entity_id = $data['service_id'];
        $request->query = QueryParser::parseAiSearchQuery($request->query);

        $saved = $this->NlpRequests->saveOrFail($request);
        $this->return = $this->NlpRequests->findById($saved->id)->contain('NlpResponses')->first();

    }
}
