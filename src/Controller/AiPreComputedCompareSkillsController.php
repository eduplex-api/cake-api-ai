<?php

declare(strict_types = 1);

namespace Ai\Controller;

use App\Controller\ApiController;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use RestApi\Lib\Exception\DetailedException;
use SbertService\Lib\SbertService;

class AiPreComputedCompareSkillsController extends ApiController
{
    public function __construct(
        SbertService $s,
        ?ServerRequest $request = null,
        ?Response $response = null,
        ?string $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry $components = null
    ) {
        $this->SbertService = $s;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function isPublicController(): bool
    {
        return false;
    }

    public function addNew($data)
    {
        $lang = $data['language'] ?? null;
        if (!is_string($lang) || strlen($lang) !== 2) {
            throw new DetailedException('language param must be ISO 639-1', 400);
        }
        $this->return = $this->SbertService->vectoriseDirectToPy('/precomputed_compare_skills/', $data);
    }
}
