<?php

declare(strict_types = 1);

namespace Ai\Controller;

use App\Controller\ApiController;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use RestApi\Lib\Exception\DetailedException;
use SbertService\Lib\QueryParser;
use SbertService\Lib\SbertService;

class AiMatchCourseSkillsController extends ApiController
{
    public function __construct(
        SbertService $s,
        ?ServerRequest $request = null,
        ?Response $response = null,
        ?string $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry $components = null
    ) {
        $this->SbertService = $s;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function isPublicController(): bool
    {
        return false;
    }

    public function addNew($data)
    {
        $lang = $data['language'] ?? null;
        if (!is_string($lang) || strlen($lang) !== 2) {
            throw new DetailedException('language param must be ISO 639-1', 400);
        }
        $sanitize = $data['sanitize_all'] ?? false;
        unset($data['sanitize_all']);
        if ($sanitize) {
            foreach ($data as $k => $elem) {
                $data[$k] = $this->removeDeStopWords(QueryParser::parseHtml($elem));
            }
        }
        $this->return = $this->SbertService->vectoriseDirectToPy('/match_course_skills/', $data);
    }

    private function removeDeStopWords(string $query): string
    {
        // phpcs:disable
        $regex = [
            '/((?<=\p{L})")|("(?=\p{L}))/',
            '/\((ca\.?\s?)?\d+(\,\d+)?\s?(UE|Tage?)\)/',
            '/(Zusammenfassung(en)?)|((Zusammenfassung(en)?)(\sund\s)(Wiederholung(en)?))|(Wiederholung(en)?)([\p{Ll}\s]+\p{Lu}\p{Ll}+)?/',
            '/Baustein((tests?)|(prüfung(en)?)|(struktur(en)?)|(einführung))?/',
            '/([Bb]austeinbezogen\s)?((Probleme?)|(\p{Lu}\p{Ll}+ziele?)|(\p{Lu}\p{Ll}+st[aä]nde?))(\s(und\s)?((messen)|(auswerten)|((über)?prüfen)|(besprechen))\,?)+/',
            '/((Durchführ)|(Auswert))\p{Ll}+\s(((\p{Ll}{3}\s)?Besprech\p{Ll}+[\s])?([\p{Ll}\s]+)?\p{Lu}\p{Ll}+(?:(prüfung(en)?)|(tests?)|(kontrollen?)))?/',
            '/(Übung(en)?\sund\s)?Fall((beispiele?)|(studien?))?(\sund\s(Aufgaben?)|(Übung(en)?))?(\sfür\s(\p{Ll}+\s)?\p{Lu}\p{Ll}+\swiederholen)?/',
            '/Arbeitsmarktprofile? der Teilnehme(r|nden) (weiter)?entwickeln/',
            '/Them\p{Ll}+\sÜbungsaufgaben?(\sFallbeispiele?)?/',
            '/[\p{L}\-]+?[Ee]inführung/',
            '/((Aust)|(Train)|(Klär)|(Übun)|(Bespr))\p{Ll}+\s\p{Ll}+\s\p{Lu}\p{Ll}+/',
            '/([Tt]hemenbezogene\s)?(Übung(en)?)|((Übung(en)?)(\sund\s)(Fallbeispiele?))|(Fallbeispiele?)/',
            '/Themenfeld\s((Klausur)|(Projekt)|(Test))+/',
            '/(Bausteinbezogene\s)?((Lernerfolgsermittlung)|(Übungen))/',
            '/(Gruppengerechte\s)?Trainings((übung(en)?)|(aufgaben?))/',
            '/(nur\sbei\sPräsenzkursen)/',
            '/\d+\s(\-\s\d+\s)?Teams bilden/',
            '/\p{Lu}?\p{Ll}[Qq]ualif\p{Ll}+?(\s\p{Ll}+)?/',
            '/\p{Lu}?\p{Ll}*[Tt]raini\p{Ll}+?(\s\p{Ll}+\s\p{Lu}\p{Ll}+)?/',
            '/(Projektarbeit(en)??)|(Fall((beispiele?)|(studien?))?)|(Präsentation(en)?)|(Übungsaufgaben?)|(\p{Lu}\p{Ll}prüfung(en)?)/',
            '/Zertifikatsvergabe\sund\sFeedbackgabe/',
            '/(Lern((stands)|(erfolgs)))\p{Ll}+/',
            '/(Abschließende?\s)?Arbeitsergebnisse\s((dokumentieren|besprechen))/',
        ];
        // phpcs:enable
        return preg_replace($regex, '', $query);
    }
}
