<?php
declare(strict_types=1);

namespace Ai\Controller;

use Ai\Lib\AwsBedrockClient;
use App\Controller\ApiController;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Event\EventManagerInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use RestApi\Lib\Exception\DetailedException;
use SbertService\Lib\SbertService;


class AiVectoriseController extends ApiController
{
    public function __construct(
        SbertService $s,
        ?ServerRequest $request = null,
        ?Response $response = null,
        ?string $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry $components = null
    ) {
        $this->SbertService = $s;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function isPublicController(): bool
    {
        return false;
    }

    protected function addNew($data)
    {
        $query = $data['vectorise'] ?? null;
        if (!is_array($query) || $query === []) {
            throw new DetailedException('vectorise param must be array', 400);
        }
        $payload = ['vectorise' => $query];
        if (Configure::read('AiPlugin.sbertServiceDisabled')) {
            $this->return = $this->_vectoriseWithAwsBedrock($query);
        } else {
            $this->return = $this->SbertService->vectoriseDirectToPy('/vectorise/', $payload);
        }
    }

    private function _vectoriseWithAwsBedrock(array $query): array
    {
        $bedrockClient = new AwsBedrockClient();
        $dimension = 1024;
        $toRet = [];
        foreach ($query as $inputText) {
            $toRet[] = [
                'vector' => $bedrockClient->invokeModel($inputText, $dimension)->getEmbedding(),
                'text' => $inputText,
                'dimension' => $dimension,
                'llm' => $bedrockClient->getModelId(),
                'language' => ''
            ];
        }
        return $toRet;
    }
}
