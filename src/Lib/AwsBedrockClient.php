<?php

namespace Ai\Lib;

use Aws\BedrockRuntime\BedrockRuntimeClient;
use Cake\Http\Exception\InternalErrorException;

class AwsBedrockClient
{
    private BedrockRuntimeClient $client;

    public function __construct()
    {
        $region = 'eu-central-1';// some models are only available in some regions
        $accessKey = $_SERVER['ACCESS_KEY_S3_SQS'] ?? '';
        if (!$accessKey) {
            throw new InternalErrorException('.env ACCESS_KEY_S3_SQS is required');
        }
        $secret = $_SERVER['SECRET_S3_SQS'] ?? '';
        if (!$secret) {
            throw new InternalErrorException('.env SECRET_S3_SQS is required');
        }

        $options = [
            'region' => $region,
            'version' => 'latest',
            'credentials' => [
                'key' => $accessKey,
                'secret' => $secret,
            ]
        ];
        $this->client = new BedrockRuntimeClient($options);
    }

    public function getModelId(): string
    {
        return 'amazon.titan-embed-text-v2:0';
    }

    public function invokeModel(string $inputText, int $dimension): BedrockResult
    {
        $options = [
            'modelId' => $this->getModelId(),
            'body' => json_encode([
                'inputText' => $inputText,
                'dimensions' => $dimension,
                'normalize' => true,
            ]),
            'contentType' => 'application/json',
            'accept' => '*/*'
        ];
        $result = $this->client->invokeModel($options);
        return new BedrockResult($result);
    }
}
