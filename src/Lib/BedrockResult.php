<?php

namespace Ai\Lib;

use Cake\Http\Exception\InternalErrorException;

class BedrockResult
{
    private $json;

    public function __construct(\Aws\Result $result)
    {
        /** @var \GuzzleHttp\Psr7\Stream $bodyStream */
        $bodyStream = $result->get('body');
        $this->json = json_decode($bodyStream->__toString(), true);

        if (!$this->json) {
            throw new InternalErrorException('Invalid json BedrockResult');
        }
    }

    public function getInputTextTokenCount(): int
    {
        return $this->json['inputTextTokenCount'];
    }

    public function getEmbedding(): array
    {
        return $this->json['embedding'];
    }

    public function getEmbeddingsByType(): array
    {
        return $this->json['embeddingsByType'];
    }
}
